###  Whole exome sequencing germline single sample SNV calling WDL pipeline

## Task 'fastq_quality_assessment'
# Used to generate reports for the quality assessment of FastQ files
task fastq_quality_assessment {
  String docker_repository
  File read1_fastq
  File read2_fastq
  Int threads = 2

  runtime {
    docker: docker_repository + "fastqc"
  }
  command {
    fastqc --outdir . -t ${threads} ${read1_fastq} ${read2_fastq}
  }
  output {
    Array[File] fastqc_htmls = glob("*.html")
  }
}

## Task 'alignment'
# Used for the alignment of raw paired-end sequences to a reference genome.
task alignment {
  String docker_repository
  String filename_prefix
  File read1_fastq
  File read2_fastq
  File genome_fasta
  File genome_fasta_index
  File bwa_index_amb
  File bwa_index_ann
  File bwa_index_bwt
  File bwa_index_pac
  File bwa_index_sa
  Int threads
  String rg_id
  String rg_sample
  String rg_platform
  String rg_unit
  String rg_library

  runtime {
    docker: docker_repository + "bwa"
  }
  command {
    bwa mem -M -t ${threads} \
      -R "@RG\tID:${rg_id}\tSM:${rg_sample}\tPL:${rg_platform}\tPU:${rg_unit}\tLB:${rg_library}" \
      ${genome_fasta} ${read1_fastq} ${read2_fastq} \
      > ${filename_prefix}.align.sam
  }
  output {
    File alignment_sam = "${filename_prefix}.align.sam"
  }
}

## Task 'alignment_postprocess'
# Used to convert alignment SAM file to BAM; coordinate-sort the BAM; and
# mark duplicate alignments.
task alignment_postprocess {
  String docker_repository
  File alignment_sam
  String filename_prefix

  runtime {
    docker: docker_repository + "sambamba"
  }
  command {
    sambamba view --sam-input --format=bam ${alignment_sam} \
      -o ${filename_prefix}.bam && \
    sambamba sort ${filename_prefix}.bam \
      --out=${filename_prefix}.sort.bam && \
    sambamba markdup ${filename_prefix}.sort.bam ${filename_prefix}.final.bam && \
    sambamba index ${filename_prefix}.final.bam ${filename_prefix}.final.bam.bai
  }
  output {
    File alignment_postprocess_bam = "${filename_prefix}.final.bam"
    File alignment_postprocess_bam_index = "${filename_prefix}.final.bam.bai"
  }
}

## Task 'alignment_quality_assessment'
# Used to quantitate several quality statistics from an aligned BAM file.
task alignment_quality_assessment {
  String docker_repository
  File alignment_postprocess_bam
  File alignment_postprocess_bam_index
  File genome_fasta
  File genome_fasta_index
  String filename_prefix

  runtime {
    docker: docker_repository + "picard"
  }
  command {
    java -Xmx6g -jar /usr/local/picard/picard.jar CollectMultipleMetrics \
      INPUT=${alignment_postprocess_bam} \
      OUTPUT=${filename_prefix} \
      REFERENCE_SEQUENCE=${genome_fasta}
  }
  output {
    File alignment_summary_metrics = "${filename_prefix}.alignment_summary_metrics"
    File base_distribution_by_cycle_pdf = "${filename_prefix}.base_distribution_by_cycle.pdf"
    File base_distribution_by_cycle_metrics = "${filename_prefix}.base_distribution_by_cycle_metrics"
    File insert_size_histogram_pdf = "${filename_prefix}.insert_size_histogram.pdf"
    File insert_size_metrics = "${filename_prefix}.insert_size_metrics"
    File quality_by_cycle_pdf = "${filename_prefix}.quality_by_cycle.pdf"
    File quality_by_cycle_metrics = "${filename_prefix}.quality_by_cycle_metrics"
    File quality_distribution_pdf = "${filename_prefix}.quality_distribution.pdf"
    File quality_distribution_metrics = "${filename_prefix}.quality_distribution_metrics"
  }
}

## Task 'germline_variant_calling'
# Used to perform germline variant calling, using the aligned BAM file, and
# a supplied intervals list
task germline_variant_calling {
  String docker_repository
  File alignment_postprocess_bam
  File alignment_postprocess_bam_index
  String filename_prefix
  File genome_fasta
  File genome_fasta_index
  File genome_fasta_dict
  File dbsnp_vcf
  File dbsnp_vcf_index
  File? intervals
  Int threads

  runtime {
    docker: docker_repository + "gatk"
  }
  command {
    java -Xmx6g -jar /usr/local/gatk/GenomeAnalysisTK.jar \
      --analysis_type HaplotypeCaller \
      --reference_sequence ${genome_fasta} \
      --input_file ${alignment_postprocess_bam} \
      --genotyping_mode DISCOVERY \
      -stand_call_conf 30 \
      --out ${filename_prefix}.raw_variants.vcf \
      --num_cpu_threads_per_data_thread ${threads} \
      --dbsnp ${dbsnp_vcf} \
      ${"--intervals " + intervals}
  }
  output {
     File raw_variants_vcf = "${filename_prefix}.raw_variants.vcf"
  }
}

## Task 'variant_annotation'
# Used to annotate variants called.
task variant_annotation {
  String docker_repository
  File raw_variants_vcf
  String filename_prefix

  runtime {
    docker: docker_repository + "snpeff"
  }
  command {
    java -Xmx6g -jar /usr/local/snpEff/snpEff.jar \
      -verbose GRCh37.75 ${raw_variants_vcf} \
      > ${filename_prefix}.ann_variants.vcf
  }
  output {
    File ann_variants_vcf = "${filename_prefix}.ann_variants.vcf"
  }
}

## Workflow 'wes_germline_variant_calling_single_sample'
# Performs germline variant calling on a single sample
# generated via whole exome sequencing.
workflow wes_germline_variant_calling_single_sample {
  String docker_repository
  String filename_prefix
  File read1_fastq
  File read2_fastq
  File genome_fasta
  File genome_fasta_index
  File genome_fasta_dict
  Int threads

  call fastq_quality_assessment {
    input: read1_fastq = read1_fastq, read2_fastq = read2_fastq,
      docker_repository = docker_repository
  }

  call alignment {
    input: filename_prefix = filename_prefix, read1_fastq = read1_fastq,
      read2_fastq = read1_fastq,
      genome_fasta = genome_fasta, genome_fasta_index = genome_fasta_index,
      threads=threads, docker_repository = docker_repository
  }

  call alignment_postprocess {
    input: alignment_sam = alignment.alignment_sam,
      filename_prefix = filename_prefix, docker_repository = docker_repository
  }

  call alignment_quality_assessment {
    input: alignment_postprocess_bam = alignment_postprocess.alignment_postprocess_bam,
      alignment_postprocess_bam_index = alignment_postprocess.alignment_postprocess_bam_index,
      genome_fasta = genome_fasta, genome_fasta_index = genome_fasta_index,
      filename_prefix = filename_prefix, docker_repository = docker_repository
  }

  call germline_variant_calling {
    input: alignment_postprocess_bam = alignment_postprocess.alignment_postprocess_bam,
      alignment_postprocess_bam_index = alignment_postprocess.alignment_postprocess_bam_index,
      genome_fasta = genome_fasta, genome_fasta_index = genome_fasta_index,
      genome_fasta_dict = genome_fasta_dict, filename_prefix = filename_prefix,
      threads=threads, docker_repository = docker_repository
  }

  call variant_annotation {
    input: raw_variants_vcf = germline_variant_calling.raw_variants_vcf,
      filename_prefix = filename_prefix, docker_repository = docker_repository
  }
}
